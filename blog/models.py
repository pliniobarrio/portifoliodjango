from django.db import models


# Create a Blog Model
# Title
# pub_date
# body
# image
class Blog(models.Model):
    Title = models.CharField(max_length=200)
    pub_date = models.DateTimeField(auto_now=False)
    body = models.TextField()
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.Title

    def summary(self):
        if len(self.body) > 100:
            result = self.body[:100] + '...'
        else:
            result = self.body
        return result

    def pub_date_pretty(self):
        return self.pub_date.strftime('%B, %Y')
